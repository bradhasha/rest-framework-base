package com.bradhasha.rest.framework.tests.example;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.bradhasha.rest.framework.core.managers.TestManager;

public class ExampleTest extends TestManager {
	

	public ExampleTest() {
		super();
	}

		@Test(groups = { "All", "Finished", "JIRA-ID" })
	public void test() {

		}

	@Override
	@AfterClass(alwaysRun=true)
	public void cleanup() {
		getCleanupManager();
	}
}
