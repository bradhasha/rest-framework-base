package com.bradhasha.rest.framework.core.managers;

import java.io.File;
import java.util.Arrays;

import com.gdit.jpes.restassured.core.managers.DatabaseManager;
import com.gdit.jpes.restassured.core.managers.LogManager;
import com.gdit.jpes.restassured.core.managers.ProcessManager;
import com.gdit.jpes.restassured.core.managers.ServerManager;

public class UtilitiesManager extends ProcessManager {

    private DatabaseManager dbm;
    private LogManager logManager;
    private ServerManager serverManager;

    public UtilitiesManager() {
        dbm = DatabaseManager.getInstance();
        logManager = new LogManager();
        serverManager = new ServerManager();
        createTmpDirectory();
    }

    /**
     * Get database manager instance
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return dbm;
    }

    /**
     * Get log manager instance
     *
     * @return
     */
    public LogManager getLogManager() {
        return logManager;
    }

    /**
     * Get the server manager instance
     *
     * @return
     */
    public ServerManager getServerManager() {
        return serverManager;
    }

    /*
     * Create tmp directory
     */
    private void createTmpDirectory() {
        File tmp = new File("data/tmp");
        if (!tmp.exists()) {
            tmp.mkdir();
        }
    }

    /**
     * Cleans up temp directory
     */
    public void cleanupTmpDirectory() {
        if (Boolean.valueOf(System.getProperty("defaults.auto-cleanup-tmp"))) {
            File tmp = new File("data/tmp");
            Arrays.asList(tmp.listFiles()).forEach(next -> next.delete());
        }
    }

    /**
     * @param millisecond
     */
    public void sleep(int millisecond) {
        try {
            System.out.println("Sleeping for " + millisecond + " milliseconds.");
            Thread.sleep(millisecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
