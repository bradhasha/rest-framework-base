package com.bradhasha.rest.framework.core;

import org.json.JSONArray;

import com.bradhasha.rest.framework.core.ListQuery.LogicalOperator;

/**
 * @author bradhasha
 */
public class JoinCondition extends JSONArray {
	
	public JoinCondition(LogicalOperator opr) {
		super();
		put("join");
		put(opr.name());
	}
	
	
}
