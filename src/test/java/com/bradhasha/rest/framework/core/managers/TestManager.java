package com.bradhasha.rest.framework.core.managers;

import com.bradhasha.rest.framework.core.RequestBuilder.OverrideData;
import com.bradhasha.rest.framework.helpers.Helper;
import com.gdit.jpes.restassured.core.managers.CleanupManager;
import com.gdit.jpes.restassured.core.managers.FunctionalManager;
import com.gdit.jpes.restassured.core.managers.UtilitiesManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * @author bradhasha
 */
public abstract class TestManager {

	private HashMap<OverrideData, Object> overrideData;
	private Helper helper;
	private UtilitiesManager um;
	private CleanupManager cleanupManager;
	private FunctionalManager functionalManager;

	private Logger logger;

	public abstract void cleanup();

	public TestManager() {
		final String propsFile = System.getProperty("propsFile");
		if (propsFile != null && propsFile.length() > 0){
			try (Scanner scanner = new Scanner(new File(propsFile))) {
				scanner.forEachRemaining(next -> {
					final String[] kv = next.split("=");
					System.setProperty(kv[0], kv[1]);
					System.out.println("Key: " + kv[0] + " Value: " +kv[1]);
				});
			} catch (FileNotFoundException fnfe) {

			}
		}
		overrideData = new HashMap<>();
		
		// Core
		logger = LogManager.getLogger(getClass().getSimpleName());
	/*	final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
		final Configuration config = ctx.getConfiguration();

		FileAppender append = FileAppender.newBuilder()
				.withAppend(false)
				.withFileName("data/logs/" + logger.getName()+".txt")
				.withName("LOGFILE")
				.withLayout(PatternLayout.createDefaultLayout())
				.build();

		config.getAppenders().remove("LOGFILE");
		config.addAppender(append);
		ctx.updateLoggers(config);
		ctx.reconfigure();*/

		helper = new Helper(logger);
		um = new UtilitiesManager();
		functionalManager = new FunctionalManager(helper);
		
		// Initialize more managers
		cleanupManager = new CleanupManager(overrideData, helper, um);
	}

	/**
	 * Get override data map
	 * 
	 * @return
	 */
	public HashMap<OverrideData, Object> getOverrideData() {
		return overrideData;
	}

	/**
	 * Get the helper
	 * 
	 * @return
	 */
	public Helper getHelper() {
		return helper;
	}

	/**
	 * Get utilities manager
	 * 
	 * @return
	 */
	public UtilitiesManager getUtilitiesManager() {
		return um;
	}
	
	/**
	 * Get the functional manager
	 * @return
	 */
	public FunctionalManager getFunctionalManager() {
		return functionalManager;
	}

	/**
	 * Get the cleanup helper
	 * 
	 * @return
	 */
	public CleanupManager getCleanupManager() {
		return cleanupManager;
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		getUtilitiesManager().getDatabaseManager().disconnect();
		getUtilitiesManager().cleanupTmpDirectory();
	}
}
