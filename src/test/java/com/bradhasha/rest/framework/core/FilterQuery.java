package com.bradhasha.rest.framework.core;

public class FilterQuery {

    private String filter;
    private String value;
    private Integer index;


    /**
     *
     * @param filter
     * @param value
     * @param index
     */
    public FilterQuery(String filter, String value, Integer index) {
        this.filter = filter;
        this.value = value;
        this.index = index;
    }

    /**
     *  Create a new FilterQuery object, sets index to null
     * @param filter
     * @param value
     */
    public FilterQuery(String filter, String value) {
        this(filter, value, null);
    }

    public String getFilter() {
        return filter;
    }

    public String getValue() {
        return value;
    }

    public Integer getIndex() {
        return index;
    }
}
