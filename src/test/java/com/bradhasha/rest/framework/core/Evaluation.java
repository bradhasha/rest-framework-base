package com.bradhasha.rest.framework.core;

import org.json.JSONObject;

/**
 * @author bradhasha
 */
public class Evaluation extends JSONObject {
	
	public Evaluation(final String type, final String key, final String value) {
		super();
		put("type", type);
		put("key", key);
		put("value", value);
	}
}
