package com.bradhasha.rest.framework.core.managers;

import com.gdit.jpes.restassured.core.managers.DatabaseManager;
import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseManager {

    private Connection connection = null;
    private ArrayList<ResultSet> results = null;
    private ArrayList<Statement> statements = null;

    /**
     * Singleton pattern to ease use by manager / utility / helper classes
     */
    private static DatabaseManager databaseManager;

    private DatabaseManager() {
    }

    public static DatabaseManager getInstance() {
        if (databaseManager == null) {
            databaseManager = new DatabaseManager();
        }
        return databaseManager;
    }


    /**
     * Create connection
     */
    public void connect() {
        final String server = System.getProperty("defaults.database.server");
        final String database = System.getProperty("defaults.database.database");
        final String username = System.getProperty("defaults.database_username");
        final String password = System.getProperty("defaults.database_password");
        connect(server, database, username, password);

    }

    /**
     *
     * @param database
     */
    public void connect(Database database) {
        disconnect();
        switch (database) {
            case DEV:
                connect();
                break;
        }
    }

    /**
     * @param server
     * @param database
     * @param username
     * @param password
     */
    public void connect(String server, String database, String username, String password) {
        try {
            if (connection == null || connection.isClosed()) {
                System.out.println("Attempting to connect to " + server + database + " with user " + username);
                String url = "jdbc:postgresql://" + server + database;
                connection = DriverManager.getConnection(url, username, password);
                System.out.println("Connected to database.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Close connection
     */
    public void disconnect() {
        if (connection != null) {
            try {
                // Close ResultSets
                if (results != null) {
                    results.forEach(next -> {
                        try {
                            if (next != null) {
                                next.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    });
                }
                // Close statements
                if (statements != null) {
                    statements.forEach(next -> {
                        try {
                            if (next != null) {
                                next.close();
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    });
                }
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println("Disconnected from database.");
        }
    }

    /**
     * Get connection
     *
     * @return
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Execute query and return result set positioned at first row
     *
     * @param query
     * @return
     */
    public ResultSet executeQuery(final String query) {
        Statement st = null;
        ResultSet rs = null;
        try {
            st = connection.createStatement();
            try {
                rs = st.executeQuery(query);
                rs.next();
            } catch (SQLException e1) {
            }
            System.out.println("Executed query: " + query);
        } catch (SQLException e2) {
            e2.printStackTrace();
        }

        if (results == null) {
            results = new ArrayList<>();
        }
        if (statements == null) {
            statements = new ArrayList<>();
        }
        results.add(rs);
        statements.add(st);
        return rs;
    }

    /**
     * Execute query and return CachedRowSet
     * @param query
     * @return
     */
    public CachedRowSet cachedExecuteQuery(final String query) {
        Statement st = null;
        ResultSet rs = null;
        try {
            st = connection.createStatement();
            try {
                rs = st.executeQuery(query);
            } catch (SQLException e1) {
            }
            System.out.println("Executed query: " + query);
        } catch (SQLException e2) {
            e2.printStackTrace();
        }

        if (results == null) {
            results = new ArrayList<>();
        }
        if (statements == null) {
            statements = new ArrayList<>();
        }
        results.add(rs);
        statements.add(st);
        try {
            CachedRowSet cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(rs);
            return cachedRowSet;
        } catch (SQLException e) {
            return null;
        }

    }

    public enum Database {
        DEV
    }

}
