package com.bradhasha.rest.framework.core.managers;

import org.unix4j.Unix4j;
import org.unix4j.line.Line;

import com.bradhasha.rest.framework.core.managers.ServerManager.Server;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProcessManager {

    /**
     * Execute command on remote server and return output / errors
     *
     * @param cmd
     * @param timeout
     * @param unit
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public HashMap<String, String> execute(String cmd, long timeout, TimeUnit unit) throws IOException, InterruptedException {
        System.out.println("Sending command: " + cmd);

        HashMap<String, String> retval = new HashMap<>();
        Process p = Runtime.getRuntime().exec(cmd);
        p.waitFor(timeout, unit);

        StringBuilder output = new StringBuilder();
        StringBuilder errors = new StringBuilder();

        BufferedReader outputReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader errorsReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));

        // Append each of the retrieved lines to the StringBuilder object
        outputReader.lines().forEach(next -> output.append(next + "\n"));
        errorsReader.lines().forEach(next -> errors.append(next + "\n"));

        // Close the readers
        if (outputReader != null) {
            outputReader.close();
        }

        if (errorsReader != null) {
            errorsReader.close();
        }

        // Kill process if it is still alive
        if (p.isAlive()) {
            p.destroyForcibly();
        }

        final String outputTxt = output.toString();
        final String errorsTxt = errors.toString();
        // Print output
        System.out.println(outputTxt);
        System.out.println(errorsTxt);

        retval.put("output", output.toString());
        retval.put("errors", errors.toString());
        return retval;
    }


    /**
     * SCP file from a server to the data/tmp folder
     *
     * @param server
     * @param fileLocation
     * @throws IOException
     * @throws InterruptedException
     */
    public void scpFile(Server server, String fileLocation) throws IOException, InterruptedException {
        String cmd = "data/scripts/scp.sh " + server.getAccessUser() + "@" + server.getIpAddress() + " " + server.getRequiredCertificate().get()
                + " " + fileLocation + " data/tmp";
        execute(cmd, 180, TimeUnit.SECONDS);
    }

    /**
     * Unzip a file within the data/tmp folder
     *
     * @param fileName
     * @throws IOException
     * @throws InterruptedException
     */
    public void unzipFile(String fileName) throws IOException, InterruptedException {
        String cmd = "gunzip data/tmp/" + fileName;
        execute(cmd, 60, TimeUnit.SECONDS);
    }

    /**
     * Grep a file, return a List of results
     *
     * @param searchString String to search for
     * @param filePath     Location of file to search
     * @return List
     */
    public List<Line> grepFile(String searchString, String filePath) {
        System.out.println("Searching for String: " + searchString);
        return Unix4j.grep(searchString, new File(filePath)).toLineList();
    }

}
