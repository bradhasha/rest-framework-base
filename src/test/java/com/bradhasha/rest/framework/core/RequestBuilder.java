package com.bradhasha.rest.framework.core;

import com.bradhasha.rest.framework.data.Users;
import com.bradhasha.rest.framework.helpers.misc.StringHolder;
import com.cedarsoftware.util.io.JsonWriter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gdit.jpes.restassured.core.ContentType;
import com.gdit.jpes.restassured.core.CustomJSONArray;
import com.gdit.jpes.restassured.core.CustomJSONObject;
import com.gdit.jpes.restassured.core.ListQuery;
import com.gdit.jpes.restassured.core.OverrideData;
import com.gdit.jpes.restassured.core.RequestBuilder;
import com.gdit.jpes.restassured.core.RequestData;
import com.gdit.jpes.restassured.core.RequestType;
import com.gdit.jpes.restassured.core.ToLoggerPrintStream;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.LogConfig;
import io.restassured.parsing.Parser;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author bradhasha
 */
public abstract class RequestBuilder<I extends Enum<I> & RequestBuilder.Node> {

    private RequestSpecBuilder builder = null;
    private HashMap<I, List<Object>> nodes;
    private HashMap<RequestData, Object> data;
    private RequestType requestType = null;
    private String endpoint;
    private boolean hasBody = false;
    private ContentType contentType = null;
    private static HashMap<String, Object> parentNodeHistory = new HashMap<>();
    private Logger logger;

    public abstract void setEndpoint();

    public abstract void setHasBody();

    public RequestBuilder(HashMap<I, List<Object>> nodes, RequestType requestType, ContentType defaultContentType,
                          String baseUri, HashMap<OverrideData, Object> map, Logger logger) {
        this.nodes = nodes;
        this.requestType = requestType;
        this.logger = logger;
        data = new HashMap<>();

        // Manage parent nodes and clearing to deal with caching issue
        nodes.forEach((k, v) -> {
            Object parent = k.getParent();
            if (parent instanceof CustomJSONObject) {
                parentNodeHistory.put(((CustomJSONObject) parent).getName(), parent);
            } else if (parent instanceof CustomJSONArray) {
                parentNodeHistory.put(((CustomJSONArray) parent).getName(), parent);
            }
        });

        // Clear objects from previous requests
        parentNodeHistory.entrySet().forEach(next -> {
            Object val = next.getValue();
            if (val != null) {
                if (val instanceof CustomJSONObject) {
                    ((CustomJSONObject) val).keySet().clear();
                } else {
                    ((CustomJSONArray) val).clear();
                }
            }
        });

        RestAssured.defaultParser = Parser.JSON;
        RestAssured.config = RestAssured.config()
                .logConfig(new LogConfig().defaultStream(new ToLoggerPrintStream(logger).getPrintStream()));
        builder = new RequestSpecBuilder();

        if (map != null && map.containsKey(OverrideData.BASE_URI)) {
            // Set URL with override
            builder.setBaseUri(System.getProperty("protocol") + "://" + map.get(OverrideData.BASE_URI));
        } else {
            // Set default URL
            builder.setBaseUri(System.getProperty("protocol") + "://" + baseUri);
        }

        // Build headers
        String[] userAndFormat = map != null && map.containsKey(OverrideData.USER)
                ? String.valueOf(map.get(OverrideData.USER)).toString().split("###")
                : Users.User.JPES_SERIES_FM_USER.uuid().split("###");
        builder.addHeader("x-subject-name", userAndFormat[0]);
        builder.addHeader("x-nameid-format", userAndFormat[1]);

        // Get content type
        contentType = map != null && map.containsKey(OverrideData.CONTENT_TYPE)
                ? ContentType.valueOf(map.get(OverrideData.CONTENT_TYPE).toString()) : defaultContentType;
        builder.setContentType(contentType.get());
        // builder.setAccept(io.restassured.http.ContentType.valueOf(contentType.name()));
        ContentType accpt = map != null && map.containsKey(OverrideData.ACCEPT)
                ? ContentType.valueOf(map.get(OverrideData.ACCEPT).toString()) : ContentType.JSON;
        builder.setAccept(accpt.get());

        // If a node is passed in and was set to null grab it's default values
        if (nodes != null) {
            nodes.forEach((k, v) -> {

                if (v == null) {
                    nodes.put(k, k.getDefaultValues());
                } else {
                    v.forEach(next -> {
                        if (next == null) {
                            nodes.put(k, k.getDefaultValues());
                        }
                    });
                }
            });
        }
    }

    /**
     * Build request
     *
     * @return
     */
    public RequestSpecification build() {
        RequestSpecification rs = null;
        String json = "";
        switch (contentType) {
            case JSON:
                // Add the nodes and their values to the appropriate JSONObject
                if (getHasBody()) {
                    nodes.forEach((k, v) -> {
                        if (k.getParent() != null && k.getParent() instanceof CustomJSONObject) {
                            if (v.size() >= 1) {
                                // If always array is set, create a new json array
                                // for
                                // the node so that values will always be an array
                                // even
                                // if there is only one value in the array
                                if (k.getAlwaysArray()) {
                                    JSONArray arr = new JSONArray();
                                    v.forEach(nv -> arr.put(nv));
                                    ((CustomJSONObject) k.getParent()).put(k.getNodeName(), arr);
                                } else {
                                    if (!k.getNodeName().equals("placeholder")) {
                                        ((CustomJSONObject) k.getParent()).put(k.getNodeName(),
                                                v.size() == 1 ? v.get(0) : v);
                                    }
                                }
                            }
                        } else if (k.getParent() != null && k.getParent() instanceof CustomJSONArray) {
                            v.forEach(nv -> ((CustomJSONArray) k.getParent()).put(nv));
                        }
                    });


                    // Build the request based on parent/child relationship
                    nodes.forEach((k, v) -> {
                        if (k.getParent() instanceof CustomJSONObject) {
                            CustomJSONObject next = ((CustomJSONObject) k.getParent());
                            if (next.getParent() != null) {
                                Object p = nodes.keySet().stream().filter(nextNode -> {
                                    Object parent = nextNode.getParent();
                                    if (parent != null) {
                                        if (parent instanceof CustomJSONObject) {
                                            return ((CustomJSONObject) parent).getName().equals(next.getParent());
                                        } else {
                                            return ((CustomJSONArray) parent).getName().equals(next.getParent());
                                        }
                                    }
                                    return false;
                                }).findFirst().get().getParent();

                                if (p instanceof CustomJSONObject) {
                                    ((CustomJSONObject) p).put(next.getName(), next);
                                } else {
                                    ((CustomJSONArray) p).put(new JSONObject().put(next.getName(), next));
                                }
                            }
                        } else if (k.getParent() != null) {
                            CustomJSONArray next = ((CustomJSONArray) k.getParent());
                            if (next.getParent() != null) {
                                Object p = nodes.keySet().stream().filter(nextNode -> {
                                    Object parent = nextNode.getParent();
                                    if (parent instanceof CustomJSONObject) {
                                        return ((CustomJSONObject) parent).getName().equals(next.getParent());
                                    } else {
                                        return ((CustomJSONArray) parent).getName().equals(next.getParent());
                                    }
                                }).findFirst().get().getParent();

                                if (p instanceof CustomJSONObject) {
                                    ((CustomJSONObject) p).put(next.getName(), next);
                                } else {
                                    ((CustomJSONArray) p).put(new JSONObject().put(next.getName(), next));
                                }
                            }
                        }
                    });

                    // Add request to data map, get json for debugging later
                    Object obj = nodes.keySet().stream()
                            .filter(next -> next.getParent() instanceof CustomJSONObject
                                    ? ((CustomJSONObject) next.getParent()).getParent() == null
                                    : next.getParent() instanceof CustomJSONArray
                                    ? ((CustomJSONArray) next.getParent()).getParent() == null : false)
                            .findFirst().get().getParent();
                    json = JsonWriter.formatJson(obj.toString());

                    // Add request data to data map in various formats
                    try {
                        data.put(RequestData.REQUEST_JSON_OBJECT, new CustomJSONObject(json));
                    } catch (JSONException e) {
                        data.put(RequestData.REQUEST_JSON_OBJECT, new CustomJSONObject("{ \"array\": " + json + " }"));
                    }
                    data.put(RequestData.REQUEST_JSONPATH_OBJECT, new JsonPath(json));
                    final HashMap<String, Object> nodeKeyValuePairs = new HashMap<>();
                    nodes.forEach((k, v) -> {
                        nodeKeyValuePairs.put(k.getNodeName(), v.size() == 1 ? v.get(0) : v);
                    });
                    data.put(RequestData.REQUEST_KEYVALUE_PAIRS, nodeKeyValuePairs);

                    // Set the request body and return the built
                    // RequestSpecification
                    // object
                    builder.setBody(json);
                }
                rs = builder.build();
                break;
            case MULTIPART_FORM_DATA:
                RequestSpecification mp = builder.build().given();
                nodes.forEach((k, v) -> mp.multiPart(k.getNodeName(), v.get(0)));
                rs = mp;
                break;
            case X_WWW_FORM_URLENCODED:
                RequestSpecification wwwForm = builder.build().given();
                // Add nodes and values to encoded form
                nodes.forEach((k, v) -> {
                    // Add list query options
                    IntStream.range(0, v.size()).forEach(next -> {
                        Object n = v.get(next);
                        if (n instanceof ListQuery) {
                            ((ListQuery) n).get().forEach((k1, v1) -> {
                                if (k1.equalsIgnoreCase(ListQuery.ListQueryKey.EXCLUSIONS.get())) {
                                    wwwForm.formParam("listQuery[" + k1 + "][]", v1);
                                } else {
                                    wwwForm.formParam("listQuery[conditions][" + next + "][" + k1 + "]", v1);
                                }
                            });
                        } else if (n instanceof FilterQuery) {
                            FilterQuery filterQuery = (FilterQuery) n;
                            // If an index has been set, use it
                            String index = "";
                            if (filterQuery.getIndex() != null) {
                                index = "[" + filterQuery.getIndex() + "]";
                            }
                            // Build form param
                            wwwForm.formParam("filter[" + filterQuery.getFilter() + "]" + index, filterQuery.getValue());
                        } else {
                            if (!k.getNodeName().equalsIgnoreCase("placeholder")) {
                                wwwForm.formParam(k.getNodeName(), v.get(0));
                            }
                        }
                    });
                });
                rs = wwwForm;
                break;
            case XML:
                StringBuilder xmlBody = new StringBuilder();
                XmlMapper xmlMapper = new XmlMapper();

                // For each node, serialize object and append to body
                nodes.forEach((k, v) -> {
                    try {
                        xmlBody.append(xmlMapper.writeValueAsString(v.get(0)));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                });

                // Add serialized XML to body
                builder.setBody(xmlBody.toString());
                rs = builder.build();
                break;
            case OCTET_STREAM:
                break;
            default:
                break;
        }
        logger.info("============================================================\nRequest sent: "
                + this.getClass().getSimpleName() + "\n============================================================");
        rs.log().all(true);
        return rs;
    }

    /**
     * Set the endpoint for the request
     *
     * @param endpoint
     */
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * Return the endpoint for the request
     *
     * @return
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * Set the hasBody boolean
     *
     * @param
     */
    public void setHasbody(boolean hasBody) {
        this.hasBody = hasBody;
    }

    /**
     * Return hasBody
     *
     * @return
     */
    public boolean getHasBody() {
        return hasBody;
    }

    /**
     * Execute the request against the endpoint
     *
     * @return
     */
    public HashMap<RequestData, Object> execute() {
        RequestSpecification spec = RestAssured.given().spec(build()).when();
        Response resp = null;
        switch (requestType) {
            case DELETE:
                resp = spec.delete(endpoint);
                break;
            case GET:
                resp = spec.get(endpoint);
                break;
            case POST:
                resp = spec.post(endpoint);
                break;
            case PUT:
                resp = spec.put(endpoint);
                break;
            case HEAD:
                resp = spec.head(endpoint);
                break;
        }
        data.put(RequestData.RESPONSE_BYTE_ARRAY, resp.getBody().asByteArray());
        final String respString = resp.getBody().asString();
        if (respString.length() > 0) {
            try {
                data.put(RequestData.RESPONSE_JSON_STRING, respString);
                data.put(RequestData.RESPONSE_JSON_OBJECT, new CustomJSONObject(respString));
            } catch (JSONException e) {
                JSONObject obj = new JSONObject();
                // Since creation of JSONObject failed, check if it could be a JSONArray
                if (respString.trim().startsWith("[") && respString.trim().endsWith("]")) {
                    obj.put("array", new JSONArray(respString));
                }
                data.put(RequestData.RESPONSE_JSON_OBJECT, obj);
            }
            data.put(RequestData.RESPONSE_JSONPATH_OBJECT, new JsonPath(respString));
        }
        data.put(RequestData.RESPONSE_VALIDATABLE_RESPONSE, resp.then());
        data.put(RequestData.RESPONSE_PLAIN_TEXT, respString);

        // Log response
        logger.info("============================================================\nResponse received: "
                + this.getClass().getSimpleName() + "\n============================================================");

        // Don't log the compressed file
        if (this.getClass().getSimpleName().equals("DownloadTPFDDArchive") && resp.getStatusCode() == 200) {
            System.out.println(StringHolder.fileImage);
        } else {
            resp.then().log().all(true);

        }

        return data;
    }

    /**
     * Get node key value pairs for request
     *
     * @return
     */
    public HashMap<I, List<Object>> getNodes() {
        return nodes;
    }

    /**
     * Gets the service object that will contain request/response
     *
     * @return
     */
    public HashMap<RequestData, Object> get() {
        return data;
    }

    /**
     * Interface for each request to implement to store all node information
     *
     * @author bradhasha
     */
    public interface Node {
        public String getNodeName();

        public List<Object> getDefaultValues();

        public Object getParent();

        public boolean getAlwaysArray();
    }

    public enum RequestType {
        PUT, POST, GET, DELETE, HEAD;
    }

    public enum RequestData {
        RESPONSE_BYTE_ARRAY,
        REQUEST_ORIGINAL_REQUEST,
        REQUEST_ORIGINAL_RESPONSE,
        REQUEST_JSON_OBJECT,
        REQUEST_JSONPATH_OBJECT,
        REQUEST_KEYVALUE_PAIRS,
        RESPONSE_JSON_STRING,
        RESPONSE_JSON_OBJECT,
        RESPONSE_JSONPATH_OBJECT,
        RESPONSE_VALIDATABLE_RESPONSE,
        RESPONSE_PLAIN_TEXT;
    }

    public enum OverrideData {
        CONTENT_TYPE, USER, ACCEPT, BASE_URI
    }

    public enum ContentType {
        PLAIN(
                "text/plain"),
        XML(
                "application/xml"),
        JSON(
                "application/json"),
        MULTIPART_FORM_DATA(
                "multipart/form-data"),
        X_WWW_FORM_URLENCODED(
                "application/x-www-form-urlencoded"),
        OCTET_STREAM(
                "application/octet-stream"),
        ANY(
                "*/*"
        );

        private final String contentType;

        private ContentType(final String contentType) {
            this.contentType = contentType;
        }

        public final String get() {
            return contentType;
        }
    }

}
