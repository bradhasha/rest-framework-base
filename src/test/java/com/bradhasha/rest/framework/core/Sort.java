package com.bradhasha.rest.framework.core;

import org.json.JSONObject;

import com.gdit.jpes.restassured.core.Direction;

/**
 * @author bradhasha
 */
public class Sort extends JSONObject {
	
	public Sort(final String value, final Direction dir) {
		super();
		put("value", value);
		put("direction", dir.name());
	}
	
	public enum Direction {
		ASC,
		DESC;
	}
}
