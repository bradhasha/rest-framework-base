package com.bradhasha.rest.framework.core.managers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.bradhasha.rest.framework.core.managers.ServerManager.Server;
import com.gdit.jpes.restassured.core.managers.Log;

public class LogManager {

	private final String LOG_DIRECTORY = System.getProperty("log.directory");
	private final int DEFAULT_LINES_TO_RETRIEVE = 100;

	/**
	 * Get default log with default return of 500 lines
	 * 
	 * @param log
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public final String getOutput(Log log) throws IOException, InterruptedException {
		return getOutput(log, null, DEFAULT_LINES_TO_RETRIEVE);
	}

	/**
	 * Get default log with specified # of lines
	 * 
	 * @param log
	 * @param lines
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public final String getOutput(Log log, int lines) throws IOException, InterruptedException {
		return getOutput(log, null, lines);
	}

	/**
	 * Get log other than default from log location
	 * 
	 * @param log
	 * @param logName
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public final String getOutput(Log log, final String logName) throws IOException, InterruptedException {
		return getOutput(log, logName, DEFAULT_LINES_TO_RETRIEVE);
	}

	/**
	 * Get log from log location with specified # of lines
	 * 
	 * @param log
	 * @param logName
	 * @param lines
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public final String getOutput(Log log, final String logName, int lines) throws IOException, InterruptedException {
		final String cmd = "ssh -tt -i " + log.getServer().getRequiredCertificate().get() + " ec2-user@" + log.getServer().getIpAddress()
				+ " sudo tail -n " + lines + " " + LOG_DIRECTORY + (logName == null ? log.get() : log.get(logName));
		System.out.println("Sending command: " + cmd);

		Process p = Runtime.getRuntime().exec(cmd);
		p.waitFor();

		StringBuilder sb = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

		// Append each of the retrieved lines to the StringBuilder object
		reader.lines().forEach(next -> sb.append(next + "\n"));

		// Close buffered reader
		if (reader != null) {
			reader.close();
		}

		// Kill process if it is still alive
		if (p.isAlive()) {
			p.destroyForcibly();
		}
		return sb.toString();
	}

	public enum Log {
		USER(
			"user",
			"user.log",
			Server.JFW_DEV_APP);

		private final String FOLDER;
		private final String NAME;
		private final Server SERVER;

		private Log(final String LOG_LOCATION, final String NAME, final Server SERVER) {
			this.FOLDER = LOG_LOCATION;
			this.NAME = NAME;
			this.SERVER = SERVER;
		}

		public final String get() {
			return "/" + FOLDER + "/" + NAME;
		}

		public final String get(String name) {
			return "/" + FOLDER + "/" + name;
		}

		public final Server getServer() {
			return SERVER;
		}
	}
}
