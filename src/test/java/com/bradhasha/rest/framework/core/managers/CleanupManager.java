package com.bradhasha.rest.framework.core.managers;

import static org.hamcrest.Matchers.equalTo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.bradhasha.rest.framework.core.RequestBuilder.OverrideData;
import com.bradhasha.rest.framework.core.RequestBuilder.RequestData;
import com.bradhasha.rest.framework.data.Users;
import com.bradhasha.rest.framework.helpers.Helper;
import com.gdit.jpes.restassured.core.managers.UtilitiesManager;

import io.restassured.response.ValidatableResponse;

public class CleanupManager {

	private Helper helper;
	private UtilitiesManager um;
	private HashMap<OverrideData, Object> overrideData;

	public CleanupManager() {

	}

	public CleanupManager(HashMap<OverrideData, Object> overrideData, Helper helper, UtilitiesManager um) {
		this.overrideData = overrideData;
		this.helper = helper;
		this.um = um;
	}

	/**
	 * Get helper
	 * 
	 * @return
	 */
	private Helper getHelper() {
		return helper;
	}

	/**
	 * Get utilities manager
	 * 
	 * @return
	 */
	private UtilitiesManager getUtilitiesManager() {
		return um;
	}

	/**
	 * Get override data
	 * 
	 * @return
	 */
	private HashMap<OverrideData, Object> getOverrideData() {
		return overrideData;
	}

}
