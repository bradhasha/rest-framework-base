package com.bradhasha.rest.framework.core.managers;

import com.gdit.jpes.restassured.core.managers.Certificate;
import com.gdit.jpes.restassured.core.managers.ProcessManager;

public class ServerManager extends ProcessManager {

	public enum Server {
		JFW_DEV_APP(
			"ec2-user",
			System.getProperty("microservice.service"),
			Certificate.TOOLS);

		private final String ACCESS_USER;
		private final String IP_ADDRESS;
		private final Certificate REQUIRED_CERTIFICATE;

		private Server(final String ACCESS_USER, final String IP_ADDRESS, final Certificate REQUIRED_CERTIFICATE) {
			this.ACCESS_USER = ACCESS_USER;
			this.IP_ADDRESS = IP_ADDRESS;
			this.REQUIRED_CERTIFICATE = REQUIRED_CERTIFICATE;
		}
		
		public final String getAccessUser() {
			return ACCESS_USER;
		}

		public final String getIpAddress() {
			return IP_ADDRESS.split(":")[0];
		}

		public final Certificate getRequiredCertificate() {
			return REQUIRED_CERTIFICATE;
		}
	}

	public enum Certificate {

		TOOLS(
			"tools.pem.cer");

		private final String NAME;

		private Certificate(final String NAME) {
			this.NAME = NAME;
		}

		public final String get() {
			return "data/certs/" + NAME;
		}

	}

}
