package com.bradhasha.rest.framework.core;

import java.util.HashMap;

import com.gdit.jpes.restassured.core.ComparisonOperator;
import com.gdit.jpes.restassured.core.ListQueryKey;
import com.gdit.jpes.restassured.core.LogicalOperator;

/**
 * @author bradhasha
 */
public class ListQuery {

	private HashMap<String, Object> data = new HashMap<>();

	public ListQuery() {

	}

	/**
	 * Add key and value to list query
	 * 
	 * @param key
	 * @param value
	 */
	public void add(String key, Object value) {
		data.put(key, value instanceof LogicalOperator ? LogicalOperator.valueOf(String.valueOf(value))
				: value instanceof ComparisonOperator ? ComparisonOperator.valueOf(String.valueOf(value)) : value);
	}

	/**
	 * Add key and value to list query
	 * 
	 * @param key
	 * @param value
	 */
	public void add(ListQueryKey key, Object value) {
		add(key.get(), value);
	}

	public enum ListQueryKey {

		FIELD_NAME(
			"fieldName"),
		COMPARISON_OPERATOR(
			"comparisonOperator"),
		VALUE(
			"value"),
		LOGICAL_OPERATOR(
			"logicalOperator"),
		HSTORECOL(
			"hstorecol"),
		EXCLUSIONS(
			"exclusions");

		private final String name;

		private ListQueryKey(final String name) {
			this.name = name;
		}

		/**
		 * Return name of key
		 * 
		 * @return
		 */
		public final String get() {
			return name;
		}
	}

	public HashMap<String, Object> get() {
		return data;
	}

	public enum ComparisonOperator {
		EQUALS, HSTORE, NOT_EQUALS, LESS_THAN, LESS_THAN_OR_EQUAL, GREATER_THAN, GREATER_THAN_OR_EQUAL, LIKE
	}

	public enum LogicalOperator {
		AND, OR
	}
}
