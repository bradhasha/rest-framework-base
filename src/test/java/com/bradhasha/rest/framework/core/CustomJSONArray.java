package com.bradhasha.rest.framework.core;

import org.json.JSONArray;

/**
 * @author bradhasha
 */
public class CustomJSONArray extends JSONArray {
	
	private String name = null;
	private String parent = null;
	
	public CustomJSONArray(final String name, final String parent) {
		super();
		this.name = name;
		this.parent = parent;
	}

	/**
	 * Get name of object
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Get parent to object
	 * @return
	 */
	public String getParent() {
		return this.parent;
	}

	public void clear(){
		for (int i = 0; i < this.length();i ++){
			this.remove(i);
		}
	}


}
