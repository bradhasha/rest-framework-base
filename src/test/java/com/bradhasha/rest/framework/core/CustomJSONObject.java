package com.bradhasha.rest.framework.core;

import org.json.JSONObject;

/**
 * @author bradhasha
 */
public class CustomJSONObject extends JSONObject {
	
	private String name = null;
	private String parent = null;
	
	public CustomJSONObject(final String json) {
		super(json);
	}
	
	public CustomJSONObject(final String name, final String parent) {
		super();
		this.name = name;
		this.parent = parent;
	}

	/**
	 * Get name of object
	 * @return
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Get parent to object
	 * @return
	 */
	public String getParent() {
		return this.parent;
	}
}
