package com.bradhasha.rest.framework.data;

import com.gdit.jpes.restassured.data.UserFormat;

/**
 * @author bradhasha
 */
public abstract class Users {

    public static String get(final String uid, UserFormat userFormat) {
        return uid + "###" + userFormat.getFormat();
    }

    public enum User {

        USER(
                "3671851b-c61e-4be1-ac7d-99d6a002ecf4");
   
        private final String UUID;

        private User(final String UUID) {
            this.UUID = UUID;
        }

        public String getUUID() {
            return this.UUID;
        }

        public String uuid() {
            return this.UUID + "###" + UserFormat.UUID.getFormat();
        }
    }

    public enum UserFormat {

        UUID(
                "urn:mil.disa.gov:uuid");
    	
        private final String FORMAT;

        private UserFormat(final String FORMAT) {
            this.FORMAT = FORMAT;
        }

        public String getFormat() {
            return FORMAT;
        }
    }

}
