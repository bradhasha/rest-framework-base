package com.bradhasha.rest.framework.helpers;

import com.bradhasha.rest.framework.core.RequestBuilder.OverrideData;
import com.bradhasha.rest.framework.core.RequestBuilder.RequestData;
import com.bradhasha.rest.framework.core.managers.ProcessManager;
import com.bradhasha.rest.framework.core.managers.UtilitiesManager;
import com.bradhasha.rest.framework.requests.jobs.GetJob;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gdit.jpes.restassured.data.ILOC;
import com.gdit.jpes.restassured.data.Itinerary;
import com.gdit.jpes.restassured.helpers.JobStatus;
import com.gdit.jpes.restassured.requests.jobs.GetJobs;
import com.gdit.jpes.restassured.requests.jobs.GetMassOperationJobStatus;
import com.gdit.jpes.restassured.requests.jobs.GetMassOperationJobs;
import com.gdit.jpes.restassured.requests.pids.DeleteRqmts;
import com.gdit.jpes.restassured.requests.shared.LinkRequirements;
import com.google.gson.Gson;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SharedHelper extends ProcessManager {

	private final int CHECK_JOB_STATUS_MAX = Integer.valueOf(System.getProperty("defaults.checkJobStatusMax"));
	private Logger logger;

	public SharedHelper(Logger logger) {
		this.logger = logger;
	}

	/**
	 * Get details of a job
	 * 
	 * @param jobId
	 * @param map
	 * @return
	 */
	public HashMap<RequestData, Object> getJob(final String jobId, HashMap<OverrideData, Object> map) {
		HashMap<GetJob.Node, List<Object>> nodes = new HashMap<>();

		nodes.put(GetJob.Node.JOB_ID, Arrays.asList(jobId));

		GetJob getJob = null;
		HashMap<RequestData, Object> resp = null;
		String jobStatus = "";
		int count = 0;
		do {
			getJob = new GetJob(nodes, map, logger);
			resp = getJob.execute();
			jobStatus = ((JSONObject) resp.get(RequestData.RESPONSE_JSON_OBJECT)).getJSONObject("jobStatus")
					.getString("statusSummary");
			count++;
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e){

			}
		} while (jobStatus.equals(JobStatus.PROCESSING.name()) && count <= getCheckJobStatusMax());
		return resp;
	}


	/*
	 * Handle altering default user for a request
	 */
	public HashMap<OverrideData, Object> alterDefaultUser(final String user, HashMap<OverrideData, Object> map) {
		if (map != null && !map.containsKey(OverrideData.USER)) {
			map.put(OverrideData.USER, user);
		} else if (map == null) {
			map = new HashMap<>();
			map.put(OverrideData.USER, user);
		}
		return map;
	}

	/**
	 * Return the maximum number of times to check the job status
	 * 
	 * @return
	 */
	public final int getCheckJobStatusMax() {
		return CHECK_JOB_STATUS_MAX;
	}

	/**
	 * Process results helper method
	 * 
	 * @param result
	 * @param resultOutput
	 */
	public void processResults(HashMap<String, Boolean> result, HashMap<String, String> resultOutput) {
		// Output pass
		System.out.println("------------------------------FAILED");
		result.entrySet().stream().filter(next -> !next.getValue()).collect(Collectors.toList()).forEach(next -> {
			System.out.println(next.getKey() + " -> " + resultOutput.get(next.getKey()));
		});

		System.out.println("\n----------------------------PASSED");
		result.entrySet().stream().filter(next -> next.getValue()).collect(Collectors.toList()).forEach(next -> {
			System.out.println(next.getKey() + " -> " + resultOutput.get(next.getKey()));
		});

		result.forEach((k, v) -> {
			Assert.assertTrue(v);
		});
	}

	/**
	 * Compare two json objects
	 * @param obj1
	 * @param obj2
	 * @return
	 */
	public boolean compareJSONObjects(JSONObject obj1, JSONObject obj2) {

		final ObjectMapper mapper = new ObjectMapper();

		JsonNode tree1 = null;
		JsonNode tree2 = null;
		try {
			tree1 = mapper.readTree(obj1.toString());
			tree2 = mapper.readTree(obj2.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

		tree1.iterator().forEachRemaining(next -> System.out.println(next.asText()));
		tree2.iterator().forEachRemaining(next -> System.out.println(next.asText()));
		return tree1 != null && tree2 != null ? tree1.equals(tree2) : false;
	}

	public boolean findMatchInJSONArray_JSONObject(JSONArray array, String key, String value) {
		return array.length() == 0 ? false : array.toList().stream().anyMatch(next -> {
			JSONObject test = new JSONObject(new Gson().toJson(next));
			System.out.println(test);
			return String.valueOf(test.get(key)).equalsIgnoreCase(value);
		});
	}

	public boolean findMatchInJSONArray_JSONObject(JSONArray array, HashMap<String,String> kv) {
        if (array.length() != 0) {
           return kv.entrySet().parallelStream().allMatch(entry -> {
                return array.toList().stream().anyMatch(next -> {
                    JSONObject test = new JSONObject(new Gson().toJson(next));
                    System.out.println(test);
                    return String.valueOf(test.get(entry.getKey())).equalsIgnoreCase(entry.getValue());
                });
            });
        }
        return false;
	}

	public enum JobStatus {
		SUCCESS, ERROR, RUNNING, PROCESSING, IN_PROGRESS, QUEUED, WARNING, FAILED;
	}

}
