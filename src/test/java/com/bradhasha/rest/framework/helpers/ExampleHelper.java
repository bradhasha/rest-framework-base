package com.bradhasha.rest.framework.helpers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.bradhasha.rest.framework.core.RequestBuilder.OverrideData;
import com.bradhasha.rest.framework.core.RequestBuilder.RequestData;
import com.bradhasha.rest.framework.requests.examples.ExampleRequestObject;
import com.gdit.jpes.restassured.helpers.SharedHelper;

/**
 * @author bradhasha
 */
public class ExampleHelper extends SharedHelper {
	
	private Logger logger;
	
	public ExampleHelper(Logger logger) {
		super(logger);
		this.logger = logger;
	}

	/**
	 * Retrieve tpfdd audit data for a given plan and audit table
	 * 
	 * @param pid
	 * @param auditTable
	 * @param start
	 * @param length
	 * @param nodes
	 * @param map
	 * @return
	 */
	public HashMap<RequestData, Object> listQueryExample(final String var1,
		 final int start, final int length, final List<Object> listQueries,
			HashMap<ExampleRequestObject.Node, List<Object>> nodes, HashMap<OverrideData, Object> map) {
		// Instantiate nodes if it is null
		if (nodes == null) {
			nodes = new HashMap<>();
		}

		nodes.put(ExampleRequestObject.Node.PLAN_IDS, Arrays.asList(var1));
		nodes.put(ExampleRequestObject.Node.START, Arrays.asList(start));
		nodes.put(ExampleRequestObject.Node.LENGTH, Arrays.asList(length));
		if (listQueries != null) {
			nodes.put(ExampleRequestObject.Node.LIST_QUERIES, listQueries);
		}

		ExampleRequestObject exampleRequestObject = new ExampleRequestObject(nodes, map, logger);
		return exampleRequestObject.execute();
	}

}
