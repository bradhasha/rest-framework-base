package com.bradhasha.rest.framework.helpers;

import org.apache.logging.log4j.Logger;

/**
 * @author bradhasha
 */
public class Helper {

	private Logger logger;
	private ExampleHelper exampleHelper;


	public Helper(Logger logger) {
		this.logger = logger;
		exampleHelper = new ExampleHelper(logger);
	}
	
	public Logger getLogger() {
		return logger;
	}

	public ExampleHelper ExampleHelper() {
		return exampleHelper;
	}
}
