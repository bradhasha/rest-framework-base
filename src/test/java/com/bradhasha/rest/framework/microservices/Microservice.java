package com.bradhasha.rest.framework.microservices;

import org.apache.logging.log4j.Logger;

import com.bradhasha.rest.framework.core.RequestBuilder;
import com.gdit.jpes.restassured.core.ContentType;
import com.gdit.jpes.restassured.core.OverrideData;
import com.gdit.jpes.restassured.core.RequestType;

import java.util.HashMap;
import java.util.List;

public abstract class Microservice<I extends Enum<I> & RequestBuilder.Node> extends RequestBuilder<I> {

	private final static String BASE_URI = System.getProperty("microservice.server") + System.getProperty("microservice.endpoint");
	
	public Microservice(HashMap<I, List<Object>> nodes, RequestType requestType, ContentType defaultContentType,
							 HashMap<OverrideData, Object> map, Logger logger) {
		super(nodes, requestType, defaultContentType, BASE_URI, map, logger);
	}
}
