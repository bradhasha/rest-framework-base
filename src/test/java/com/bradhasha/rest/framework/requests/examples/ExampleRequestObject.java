package com.bradhasha.rest.framework.requests.examples;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.bradhasha.rest.framework.core.RequestBuilder;
import com.bradhasha.rest.framework.microservices.Microservice;
import com.gdit.jpes.restassured.core.ContentType;
import com.gdit.jpes.restassured.core.OverrideData;
import com.gdit.jpes.restassured.core.RequestType;

public class ExampleRequestObject extends Microservice<ExampleRequestObject.Node> {

	// Request Type
	private final static RequestType REQUEST_TYPE = RequestType.POST;
	private final static ContentType defaultContentType = ContentType.X_WWW_FORM_URLENCODED;

	public ExampleRequestObject(HashMap<Node, List<Object>> nodes,
			HashMap<OverrideData, Object> map, Logger logger) {
		super(nodes, REQUEST_TYPE, defaultContentType, map, logger);
		setEndpoint();
		setHasBody();
	}

	@Override
	public void setEndpoint() {
		super.setEndpoint("/endpoint/path");
	}

	@Override
	public void setHasBody() {
		super.setHasbody(false);
	}

	public enum Node implements RequestBuilder.Node {
		PLAN_IDS(
			"planIds[]",
			Arrays.asList(),
			null, false),
		START(
			"start",
			Arrays.asList(),
			null, false),
		LENGTH(
			"length",
			Arrays.asList(),
			null, false),
		LIST_QUERIES(
			null,
			Arrays.asList(),
			null, false);

		private final String NODE_NAME;
		private final List<Object> DEFAULT_VALUES;
		private final Object PARENT;
		private final boolean ALWAYS_ARRAY;

		private Node(final String NODE_NAME, final List<Object> DEFAULT_VALUES, final Object PARENT, final boolean ALWAYS_ARRAY) {
			this.NODE_NAME = NODE_NAME;
			this.DEFAULT_VALUES = DEFAULT_VALUES;
			this.PARENT = PARENT;
			this.ALWAYS_ARRAY = ALWAYS_ARRAY;
		}

		@Override
		public String getNodeName() {
			return NODE_NAME;
		}

		public final List<Object> getDefaultValues() {
			return DEFAULT_VALUES;
		}

		@Override
		public Object getParent() {
			return PARENT;
		}

		@Override
		public boolean getAlwaysArray() {
			return ALWAYS_ARRAY;
		}
	}
}
