package com.bradhasha.rest.framework.requests.examples;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.bradhasha.rest.framework.core.CustomJSONArray;
import com.bradhasha.rest.framework.core.CustomJSONObject;
import com.bradhasha.rest.framework.core.RequestBuilder;
import com.bradhasha.rest.framework.microservices.Microservice;
import com.gdit.jpes.restassured.core.ContentType;
import com.gdit.jpes.restassured.core.OverrideData;
import com.gdit.jpes.restassured.core.RequestType;
import com.gdit.jpes.restassured.requests.examples.DynamicJSONBuilderExample;
import com.gdit.jpes.restassured.requests.examples.Node;

public class DynamicJSONBuilderExample extends Microservice<DynamicJSONBuilderExample.Node> {

	private final static RequestType REQUEST_TYPE = RequestType.POST;
	private final static ContentType defaultContentType = ContentType.JSON;

	private static CustomJSONObject main = new CustomJSONObject("main", null);
	private static CustomJSONObject query = new CustomJSONObject("query", "main");
	private static CustomJSONObject bool = new CustomJSONObject("bool", "query");
	private static CustomJSONArray must = new CustomJSONArray("must", "bool");
	private static CustomJSONObject match = new CustomJSONObject("match", "must");
	private static CustomJSONObject tags = new CustomJSONObject("tags", "match");
	private static CustomJSONObject range = new CustomJSONObject("range", "must");
	private static CustomJSONObject timestamp = new CustomJSONObject("timestamp", "range");

	public DynamicJSONBuilderExample(HashMap<Node, List<Object>> nodes, HashMap<OverrideData, Object> map, Logger logger) {
		super(nodes, REQUEST_TYPE, defaultContentType, map, logger);
		setEndpoint();
		setHasBody();
	}

	@Override
	public void setEndpoint() {
		super.setEndpoint("/tpfdds");
	}

	@Override
	public void setHasBody() {
		super.setHasbody(true);
	}

	public enum Node implements RequestBuilder.Node {
		PLAN_ID(
			"planId",
			Arrays.asList(System.getProperty("defaults.pid")),
			main, false),
		MAIN_QUERY(
			"query",
			Arrays.asList(),
			query, false),
		BOOL(
			"bool",
			Arrays.asList(),
			bool, false),
		MUST(
			"must",
			Arrays.asList(),
			must, false),
		MATCH(
			"match",
			Arrays.asList(),
			match, false),
		TAGS(
			"tags",
			Arrays.asList(),
			tags, false),
		SUB_QUERY(
			"query",
			Arrays.asList(),
			tags, false),
		RANGE(
			"range",
			Arrays.asList(),
			range, false),
		TIMESTAMP(
			"timestamp",
			Arrays.asList(),
			timestamp, false),
		GTE(
			"gte",
			Arrays.asList(),
			timestamp, false),
		LTE(
			"lte",
			Arrays.asList(),
			timestamp, false);

		private final String NODE_NAME;
		private final List<Object> DEFAULT_VALUES;
		private final Object PARENT;
		private final boolean ALWAYS_ARRAY;

		private Node(final String NODE_NAME, final List<Object> DEFAULT_VALUES, final Object PARENT, final boolean ALWAYS_ARRAY) {
			this.NODE_NAME = NODE_NAME;
			this.DEFAULT_VALUES = DEFAULT_VALUES;
			this.PARENT = PARENT;
			this.ALWAYS_ARRAY=ALWAYS_ARRAY;
		}

		@Override
		public String getNodeName() {
			return NODE_NAME;
		}

		public final List<Object> getDefaultValues() {
			return DEFAULT_VALUES;
		}

		@Override
		public Object getParent() {
			return PARENT;
		}

		@Override
		public boolean getAlwaysArray() {
			return ALWAYS_ARRAY;
		}
	}
}
