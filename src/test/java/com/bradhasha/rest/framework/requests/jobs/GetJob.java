package com.bradhasha.rest.framework.requests.jobs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.bradhasha.rest.framework.core.RequestBuilder;
import com.bradhasha.rest.framework.microservices.Microservice;
import com.gdit.jpes.restassured.core.ContentType;
import com.gdit.jpes.restassured.core.OverrideData;
import com.gdit.jpes.restassured.core.RequestType;
import com.gdit.jpes.restassured.requests.jobs.GetJob;
import com.gdit.jpes.restassured.requests.jobs.Node;

public class GetJob extends Microservice<GetJob.Node> {

	// Request Type
	private final static RequestType REQUEST_TYPE = RequestType.GET;
	private final static ContentType defaultContentType = ContentType.JSON;

	// JSON Body objects

	public GetJob(HashMap<Node, List<Object>> nodes, HashMap<OverrideData, Object> map, Logger logger) {
		super(nodes, REQUEST_TYPE, defaultContentType, map, logger);
		setEndpoint();
		setHasBody();
	}

	@Override
	public void setEndpoint() {
		super.setEndpoint("/jobs/" + getNodes().get(Node.JOB_ID).get(0));
	}

	@Override
	public void setHasBody() {
		super.setHasbody(false);
	}

	public enum Node implements RequestBuilder.Node {
		JOB_ID(
			"jobId",
			Arrays.asList(),
			null,
			false);

		private final String NODE_NAME;
		private final List<Object> DEFAULT_VALUES;
		private final Object PARENT;
		private final boolean ALWAYS_ARRAY;

		private Node(final String NODE_NAME, final List<Object> DEFAULT_VALUES, final Object PARENT,
				final boolean ALWAYS_ARRAY) {
			this.NODE_NAME = NODE_NAME;
			this.DEFAULT_VALUES = DEFAULT_VALUES;
			this.PARENT = PARENT;
			this.ALWAYS_ARRAY = ALWAYS_ARRAY;
		}

		@Override
		public String getNodeName() {
			return NODE_NAME;
		}

		public final List<Object> getDefaultValues() {
			return DEFAULT_VALUES;
		}

		@Override
		public Object getParent() {
			return PARENT;
		}

		@Override
		public boolean getAlwaysArray() {
			return ALWAYS_ARRAY;
		}
	}
}
